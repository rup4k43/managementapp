package com.example.managementsystem.util.extensions

import android.content.Context
import android.graphics.*
import android.os.SystemClock
import android.text.*
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.example.managementsystem.R
import com.example.managementsystem.data.DataStringsModel
import java.util.*

/**
 * A Throttled OnClickListener
 * Rejects clicks that are too close together in time.
 * This class is safe to use as an OnClickListener for multiple views, and will throttle each one separately.
 *
 * @param [minimumIntervalMsec] The minimum allowed time between clicks - any click sooner than this after a previous click will be rejected
 */

inline fun View.onClickThrottledListener(
    time: Long = 2000L,
    crossinline performClick: (view: View) -> Unit,
) {
    val lastClickMap: MutableMap<View, Long> = WeakHashMap()

    this.setOnClickListener { clickedView ->
        val previousClickTimestamp = lastClickMap[clickedView]
        val currentTimestamp = SystemClock.uptimeMillis()

        lastClickMap[clickedView] = currentTimestamp
        if (previousClickTimestamp == null
            || currentTimestamp - previousClickTimestamp.toLong() > time
        ) {
            performClick(clickedView)
        }
    }


}

fun Context.showConfirmationDialog(
    title: String = resources.getString(R.string.app_name),
    message: String,
    positiveButton: String = DataStringsModel.ok.get()!!,
    negativeButton: String = DataStringsModel.cancel.get()!!,
    negativeAction: () -> Unit = {},
    positiveAction: () -> Unit,
    onCancelled: () -> Unit = {},
    isCancelable: Boolean = true,
): AlertDialog.Builder {
    val builder: AlertDialog.Builder = AlertDialog.Builder(this)
    builder.setTitle(title)
        .setCancelable(isCancelable)
        .setMessage(message)
        .setPositiveButton(positiveButton) { dialog, _ ->
            dialog.dismiss()
            positiveAction()
        }
        .setNegativeButton(negativeButton) { dialog, _ ->
            dialog.dismiss()
            negativeAction()
        }
        .setOnCancelListener {
            onCancelled()
        }
    return builder
}




