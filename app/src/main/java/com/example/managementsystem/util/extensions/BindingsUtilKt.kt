package com.example.managementsystem.util.extensions

import android.view.View
import androidx.databinding.BindingAdapter


@BindingAdapter("android:onClickThrottledListener")
fun onClickThrottledListener(view: View, action: View.OnClickListener) {
    view.onClickThrottledListener {
        action.onClick(view)
    }
}
