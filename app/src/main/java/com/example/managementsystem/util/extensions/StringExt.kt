package com.example.managementsystem.util.extensions

import androidx.core.util.PatternsCompat
import java.util.regex.Pattern

fun String.isValidEmail(): Boolean {
    return PatternsCompat.EMAIL_ADDRESS.matcher(this).matches()
}

/**
 * This regex will enforce these rules:

At least one upper case English letter, (?=.*?[A-Z])
At least one lower case English letter, (?=.*?[a-z])
At least one digit, (?=.*?[0-9])
At least one special character, (?=.*?[#?!@$%^&*-])
Minimum eight in length .{8,} (with the anchors)

 */
fun String.isValidPassword(): Boolean {
    val regexPasswordValidator: String =
        "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
    return Pattern.compile(regexPasswordValidator).matcher(this).matches()
}
