package com.example.managementsystem.util.extensions

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.drawable.TransitionDrawable
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.text.Html
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.MotionEvent
import android.view.View
import android.widget.*
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.Group
import androidx.core.content.ContextCompat
import com.example.managementsystem.R
import com.example.managementsystem.data.DataStringsModel
import kotlin.math.roundToInt

fun Context.showAlert(message: String?) {
    val builder: AlertDialog.Builder = AlertDialog.Builder(this, R.style.MyAlertDialogTheme)
    builder.setTitle(R.string.app_name)
        .setMessage(message)
        .setPositiveButton(DataStringsModel.ok.get()) { dialog, _ ->
            dialog.dismiss()
        }
        .show()
}

fun Context.showToast(message: String?) {
    Toast.makeText(this, message ?: "", Toast.LENGTH_SHORT).show()
}

fun Context.showNotCancellableAlert(
    message: String,
    actionText: String = DataStringsModel.ok.get()!!,
    action: () -> Unit = {}
) {
    val builder: AlertDialog.Builder = AlertDialog.Builder(this, R.style.MyAlertDialogTheme)
    builder.setTitle(R.string.app_name)
        .setMessage(message)
        .setPositiveButton(actionText) { dialog, _ ->
            dialog.dismiss()
            action()
        }
    val alertDialog = builder.create()
    alertDialog.setCancelable(false)
    alertDialog.show()
}

fun Context.showConfirmationDialog(
    message: String,
    positiveButton: String = DataStringsModel.ok.get()!!,
    negativeButton: String = DataStringsModel.cancel.get()!!,
    negativeAction: () -> Unit = {},
    positiveAction: () -> Unit,
    onCancelled: () -> Unit = {},
    isCancelable: Boolean = true
): AlertDialog.Builder {
    val builder: AlertDialog.Builder = AlertDialog.Builder(this)
    builder.setTitle(R.string.app_name)
        .setCancelable(isCancelable)
        .setMessage(message)
        .setPositiveButton(positiveButton) { dialog, _ ->
            dialog.dismiss()
            positiveAction()
        }
        .setNegativeButton(negativeButton) { dialog, _ ->
            dialog.dismiss()
            negativeAction()
        }
        .setOnCancelListener {
            onCancelled()
        }
    return builder
}

fun Context.showNonConfirmationDialog(
    message: String,
    positiveButton: String = DataStringsModel.ok.get()!!,
    negativeButton: String = DataStringsModel.cancel.get()!!,
    negativeAction: () -> Unit = {},
    positiveAction: () -> Unit
) {
    val builder: AlertDialog.Builder = AlertDialog.Builder(this, R.style.MyAlertDialogTheme)
    builder.setTitle(R.string.app_name)
        .setMessage(message)
        .setCancelable(false)
        .setPositiveButton(positiveButton) { dialog, _ ->
            dialog.dismiss()
            positiveAction()
        }
        .setNegativeButton(negativeButton) { dialog, _ ->
            dialog.dismiss()
            negativeAction()
        }
        .show()
}

fun Context.showPermissionRequestDialog(
    message: String,
    onOptionClicked: () -> Unit = {},
    onCancelled: () -> Unit = {}
) {
    showConfirmationDialog(
        message = message.trim(),
        positiveButton = DataStringsModel.settings.get()!!,
        negativeButton = DataStringsModel.notNow.get()!!,
        negativeAction = {
            onOptionClicked.invoke()
        },
        positiveAction = {
            onOptionClicked.invoke()
            val i = Intent()
            i.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            i.addCategory(Intent.CATEGORY_DEFAULT)
            i.data = Uri.parse("package:$packageName")
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
            i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
            startActivity(i)
        }, onCancelled = onCancelled
    ).show()
}

fun Context.showToast(message: String, length: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, length).show()
}

fun Context.showToast(message: Int, length: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, length).show()
}

fun TextView.fromHtml(html: String) {
    text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
    } else {
        @Suppress("DEPRECATION")
        Html.fromHtml(html)
    }
}

fun TextView.underline() {
    paintFlags = paintFlags or Paint.UNDERLINE_TEXT_FLAG
}

fun AlertDialog.Builder.setEditText(editText: EditText): AlertDialog.Builder {
    val container = FrameLayout(context)
    container.addView(editText)
    val containerParams = FrameLayout.LayoutParams(
        FrameLayout.LayoutParams.MATCH_PARENT,
        FrameLayout.LayoutParams.WRAP_CONTENT
    ).apply {
        topMargin = (50F / 2).toPx
        leftMargin = 70F.toInt()
        rightMargin = 70F.toInt()
    }
    container.layoutParams = containerParams

    val superContainer = FrameLayout(context).apply {
        addView(container)
    }

    setView(superContainer)

    return this
}

fun Group.setAllOnClickListener(listener: View.OnClickListener?) {
    referencedIds.forEach { id ->
        rootView.findViewById<View>(id).setOnClickListener(listener)
    }
}

val Float.toPx: Int
    get() = (this * Resources.getSystem().displayMetrics.density).roundToInt()

val Int.toPx: Int
    get() = (this * Resources.getSystem().displayMetrics.density).roundToInt()

val Int.toDp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()


/**
 * Method to check if the view contains points [x] and [y]
 */
fun View.containsPoint(x: Int, y: Int): Boolean {
    val outRect = Rect()
    val location = IntArray(2)
    getDrawingRect(outRect)
    getLocationOnScreen(location)
    outRect.offset(location[0], location[1])
    return outRect.contains(x, y)
}

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

fun TextView.setUnderLineTextClick(data: String, textColor: Int, onClick: () -> Unit = {}) {
    val spannableString = SpannableString(data)
    val startPos = spannableString.indexOf(data, 0, true)
    val endPos = startPos + data.length
    spannableString.setSpan(
        object : ClickableSpan() {
            override fun onClick(widget: View) {
                onClick.invoke()
            }

            override fun updateDrawState(ds: TextPaint) {
                ds.color = ContextCompat.getColor(context, textColor)
                ds.isUnderlineText = true
            }
        }, startPos, endPos, Spanned.SPAN_INCLUSIVE_INCLUSIVE
    )
    text = spannableString
    movementMethod = LinkMovementMethod.getInstance()
}

fun ImageView.setImageDrawableWithAnimation(@DrawableRes res: Int, duration: Int = 300) {
    val currentDrawable = drawable
    val drawable = this.context.getDrawable(res)
    if (currentDrawable == null) {
        setImageDrawable(drawable)
        return
    }

    val transitionDrawable = TransitionDrawable(
        arrayOf(
            currentDrawable,
            drawable
        )
    )
    transitionDrawable.isCrossFadeEnabled = true
    setImageDrawable(transitionDrawable)
    transitionDrawable.startTransition(duration)
}

@SuppressLint("ClickableViewAccessibility")
fun EditText.onLeftDrawableClicked(onClicked: (view: EditText) -> Unit) {
    this.setOnTouchListener { v, event ->
        var hasConsumed = false
        if (v is EditText) {
            if (event.x <= v.totalPaddingLeft) {
                if (event.action == MotionEvent.ACTION_UP) {
                    onClicked(this)
                }
                hasConsumed = true
            }
        }

        hasConsumed
    }
}

