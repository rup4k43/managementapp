package com.introdating.app.base

interface ViewHolderBinder {
    fun bind(position: Int)
}