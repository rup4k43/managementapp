package com.example.managementsystem.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.managementsystem.util.liveDataUtil.SingleLiveEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

abstract class BaseViewModel : ViewModel() {
    private var job: Job? = null
    val showToastEvent = SingleLiveEvent<String>()
    fun <T : Any?> launchWithDebounce(
        coroutineScope: CoroutineScope = viewModelScope,
        debounceTimeInMillis: Long = 400,
        codeBlock: suspend CoroutineScope.() -> T
    ) {
        job?.cancel()
        val currentJob = coroutineScope.launch {
            // add delay from second request
            if (job != null) {
                delay(debounceTimeInMillis)
            }
            codeBlock()
        }
        job = currentJob
    }

    fun showToast(message: String) {
        showToastEvent.value = message
    }
}