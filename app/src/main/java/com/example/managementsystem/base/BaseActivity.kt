package com.example.managementsystem.base

import android.os.Bundle
import android.view.MenuItem
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import com.example.managementsystem.BR
import com.example.managementsystem.data.DataStringsModel
import com.example.managementsystem.util.extensions.setupUI
import com.example.managementsystem.util.extensions.showToast
import com.example.managementsystem.util.extensions.transitionActivityFinish


abstract class BaseActivity<DATA_BINDING : ViewDataBinding, VIEW_MODEL : BaseViewModel> :
    AppCompatActivity() {
    lateinit var viewDataBinding: DATA_BINDING
    private var viewModel: VIEW_MODEL? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        performDataBinding()
        initObserver()
    }

    private fun initObserver() {
        viewModel?.showToastEvent?.observe(this@BaseActivity, Observer { showToastEvent ->
            showToastEvent?.let {
                this@BaseActivity.showToast(it)
            }
        })
    }

    private fun performDataBinding() {
        viewDataBinding = DataBindingUtil.setContentView(this, getLayoutId())
        this.viewModel = if (viewModel == null) getViewModel() else viewModel
        viewDataBinding.apply {
           setVariable(getBindingVariable(), viewModel)
            setVariable(getBindingDataStringVariable(), DataStringsModel)
            lifecycleOwner =
                this@BaseActivity// Specify the current activity as the lifecycle owner.
            executePendingBindings()
        }
        setupUI(viewDataBinding.root)
    }


    /**
     * @return layout resource id
     */
    @LayoutRes
    abstract fun getLayoutId(): Int

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    abstract fun getViewModel(): VIEW_MODEL

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    open fun getBindingVariable(): Int = BR.viewModel

    /**
     * Override for set binding variable for dataString
     *
     * @return variable id
     */
    open fun getBindingDataStringVariable(): Int = BR.dataStrings

    fun setupToolbar(
        toolbar: Toolbar,
        backButtonEnabled: Boolean = true,
    ) {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayShowTitleEnabled(false)
            setDisplayHomeAsUpEnabled(backButtonEnabled)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        transitionActivityFinish()
    }
}