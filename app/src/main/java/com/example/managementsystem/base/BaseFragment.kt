package com.example.managementsystem.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.fragment.app.Fragment
import com.example.managementsystem.data.DataStringsModel
import com.example.managementsystem.util.extensions.setupUI


abstract class BaseFragment<DATA_BINDING : ViewDataBinding, VIEW_MODEL : BaseViewModel> :
    Fragment() {
    lateinit var viewDataBinding: DATA_BINDING
    private var viewModel: VIEW_MODEL? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        performDataBinding(inflater, container)
        return viewDataBinding.root
    }

    private fun performDataBinding(inflater: LayoutInflater, container: ViewGroup?) {
        viewDataBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        this.viewModel = viewModel ?: getViewModel()
        viewDataBinding.apply {
            setVariable(getBindingVariable(), viewModel)
            setVariable(getBindingDataStringVariable(), DataStringsModel)
            lifecycleOwner = viewLifecycleOwner
            executePendingBindings()
        }
        activity?.setupUI(viewDataBinding.root)
    }

    /**
     * @return layout resource id
     */
    @LayoutRes
    abstract fun getLayoutId(): Int

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    abstract fun getViewModel(): VIEW_MODEL

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    open fun getBindingVariable(): Int = BR.viewModel

    /**
     * Override for set binding variable for dataString
     *
     * @return variable id
     */
    open fun getBindingDataStringVariable(): Int = BR.dataStrings


}