package com.introdating.app.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp


/**
 * Created by Nabin Shrestha on 8/24/21.
 */

@HiltAndroidApp
class ManagementSystemApp : Application()