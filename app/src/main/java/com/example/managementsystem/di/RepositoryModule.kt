package com.example.managementsystem.di

import android.content.Context
import com.example.managementsystem.ui.addJob.AddJobRepository
import com.example.managementsystem.ui.dashboard.DashboardRepository
import com.example.managementsystem.ui.login.LoginRepository
import com.example.managementsystem.ui.registration.RegistrationRepository
import com.introdating.app.app.ManagementSystemApp
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent


@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule{


    @Provides
    fun provideRegistrationRepository(@ApplicationContext context: Context) = RegistrationRepository(context)

    @Provides
    fun provideLoginRepository(@ApplicationContext context: Context) = LoginRepository(context)

    @Provides
    fun provideDashboardRepository(@ApplicationContext context: Context) = DashboardRepository(context)

    @Provides
    fun provideAddJobRepository(@ApplicationContext context: Context) = AddJobRepository(context)

}