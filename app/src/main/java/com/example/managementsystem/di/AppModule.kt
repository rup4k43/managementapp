package com.example.managementsystem.di

import android.content.Context
import com.introdating.app.app.ManagementSystemApp
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

/**
 * Created by Nabin Shrestha on 8/24/21.
 */

@Module
@InstallIn(SingletonComponent::class)
class AppModule{
    @Provides
    fun provideApplication(@ApplicationContext app: Context): ManagementSystemApp {
        return app as ManagementSystemApp
    }
}

