package com.example.managementsystem.ui.registration

import android.content.Context
import com.example.managementsystem.data.DBHelper
import com.example.managementsystem.ui.registration.model.UserModel
import com.example.managementsystem.util.networkUtils.DataResource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn


class RegistrationRepository(context: Context) {
    private var dbHelper: DBHelper = DBHelper(context,null)
    fun registerUser(userModel: UserModel): Flow<Long> {
        return flow {
           var response =  dbHelper.registerUser(userModel)
            emit(response)
        }.flowOn(Dispatchers.IO)

    }

}