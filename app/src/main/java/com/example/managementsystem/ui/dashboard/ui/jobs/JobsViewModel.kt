package com.example.managementsystem.ui.dashboard.ui.jobs

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.managementsystem.base.BaseViewModel
import com.example.managementsystem.ui.addJob.AddJobRepository
import com.example.managementsystem.ui.addJob.model.JobModel
import com.example.managementsystem.ui.dashboard.DashboardRepository
import com.example.managementsystem.util.GlobalVariables
import com.example.managementsystem.util.networkUtils.DataResource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class JobsViewModel @Inject constructor(
    private val dashboardRepository: DashboardRepository,
    private val addJobRepository: AddJobRepository
) : BaseViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    private var _getJobResponse: MediatorLiveData<DataResource<List<JobModel>>> = MediatorLiveData()
    val getJobResponse: LiveData<DataResource<List<JobModel>>>
        get() = _getJobResponse

    private var _applyJobResponse: MediatorLiveData<DataResource<String>> = MediatorLiveData()
    val applyJobResponse: LiveData<DataResource<String>>
        get() = _applyJobResponse

    private var _deleteJobResponse: MediatorLiveData<DataResource<String>> = MediatorLiveData()
    val deleteJobResponse: LiveData<DataResource<String>>
        get() = _deleteJobResponse

    val text: LiveData<String> = _text

    fun getJobs() {
        viewModelScope.launch {
            dashboardRepository.getJobs().collect {
                if (!it.isNullOrEmpty()) {
                    _getJobResponse.postValue(DataResource.Success(it))
                } else {
                    _getJobResponse.postValue(DataResource.Error(message = "Sorry there are no jobs at the moment."))
                }
            }
        }
    }

    fun deleteJob(id: Int) {
        viewModelScope.launch {
            dashboardRepository.deleteJob(id).collect {
                if (it > 0) {
                    _deleteJobResponse.postValue(DataResource.Success("Job Deleted Successfully."))
                } else {
                    _deleteJobResponse.postValue(DataResource.Error(message = "Failed to delete job."))
                }
            }
        }
    }

    fun applyForJob(item: JobModel) {
        viewModelScope.launch {
            dashboardRepository.applyForJob(item.id.toString(), GlobalVariables.number).collect {
                when {
                    it > 0 -> {
                        addJobRepository.updateJobDetails(jobModel = item)
                        _applyJobResponse.postValue(DataResource.Success("You have applied for the job. "))

                    }
                   it == (-3).toLong() -> {
                        _applyJobResponse.postValue(DataResource.Error(message = "You already applied for this job. "))
                    }
                    else -> {
                        _applyJobResponse.postValue(DataResource.Error(message = "Failed to apply for the job. "))
                    }
                }
            }
        }
    }




}