package com.example.managementsystem.ui.dashboard.ui.appliedJobs

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.managementsystem.databinding.ItemJobsBinding
import com.example.managementsystem.ui.addJob.model.JobModel

class AppliedJobsAdapter(
    private val jobList: List<JobModel>,
    private val listener: OnItemClickListener
) :
    RecyclerView.Adapter<AppliedJobsAdapter.AppliedJobViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppliedJobViewHolder {
        val binding = ItemJobsBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return AppliedJobViewHolder(binding)
    }

    override fun getItemCount() = jobList.size

    override fun onBindViewHolder(holder: AppliedJobViewHolder, position: Int) {
        with(holder) {
            binding.apply {
                tvStatus.visibility = View.VISIBLE
                jobModel = jobList[position]
                jobCard.setOnClickListener {
                    listener.onItemClick(jobList[position])
                }
            }
        }
    }

    inner class AppliedJobViewHolder(val binding: ItemJobsBinding) :
        RecyclerView.ViewHolder(binding.root)
}
