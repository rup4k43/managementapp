package com.example.managementsystem.ui.dashboard.ui.appliedJobs

import com.example.managementsystem.ui.addJob.model.JobModel

interface OnItemClickListener {
    fun onItemClick(item: JobModel?)
}