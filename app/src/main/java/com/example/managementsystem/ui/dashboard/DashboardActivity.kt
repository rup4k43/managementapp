package com.example.managementsystem.ui.dashboard

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.managementsystem.R
import com.example.managementsystem.databinding.ActivityDashBinding
import com.example.managementsystem.ui.addJob.AddJobActivity
import com.example.managementsystem.util.GlobalVariables
import com.example.managementsystem.util.extensions.showConfirmationDialog
import com.example.managementsystem.util.extensions.transitionActivityFinish
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class DashboardActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityDashBinding

    private val dashboardViewModel: DashboardViewModel by viewModels()

    private val userName: String? by lazy {
        intent.getStringExtra(USERNAME)
    }

    private val mobileNumber: String? by lazy {
        intent.getStringExtra(MOBILE_NUMBER)
    }

    private val isAdmin: Boolean by lazy {
        intent.getBooleanExtra(IS_ADMIN, false)
    }

    companion object {
        private const val USERNAME = "username"
        private const val MOBILE_NUMBER = "mobileNumber"
        private const val IS_ADMIN = "is_admin"
        fun start(context: Context, username: String, mobileNumber: String, isAdmin: Boolean) {
            context.run {
                startActivity(Intent().apply {
                    putExtra(USERNAME, username)
                    putExtra(MOBILE_NUMBER, mobileNumber)
                    putExtra(IS_ADMIN, isAdmin)
                    setClass(this@run, DashboardActivity::class.java)
                })
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        GlobalVariables.isAdminMode = isAdmin
        mobileNumber?.let {
            GlobalVariables.number = it
        }
        binding = ActivityDashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.appBarDash.toolbar)

        binding.appBarDash.fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
        val drawerLayout: DrawerLayout = binding.drawerLayout
        val navView: NavigationView = binding.navView
        val navController = findNavController(R.id.nav_host_fragment_content_dash)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_applied_jobs, R.id.nav_jobs
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        setNavHeaderContent()
    }

    private fun setNavHeaderContent() {
        // binding.navView.findViewById<TextView>(R.id.tv_name).text = userName
        // binding.navView.findViewById<TextView>(R.id.tv_number).text = mobileNumber
        binding.navView.getHeaderView(0).findViewById<TextView>(R.id.tv_name).text = userName
        binding.navView.getHeaderView(0).findViewById<TextView>(R.id.tv_number).text = mobileNumber
        binding.appBarDash.fab.apply {
            visibility = if (isAdmin) View.VISIBLE else View.GONE
            setOnClickListener {
                AddJobActivity.start(this@DashboardActivity)
            }
        }


    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.dash, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_dash)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> {
                showConfirmation()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showConfirmation() {
        showConfirmationDialog(
            title = "LOGOUT FROM APP",
            message = "Are you sure you want to logout",
            positiveAction = {


            },
            negativeAction = {
                finish()
                transitionActivityFinish()
            },
            negativeButton = "Yes",
            positiveButton = "No"
        ).show()
    }

    override fun onBackPressed() {

        showConfirmation()
    }
}