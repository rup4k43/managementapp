package com.example.managementsystem.ui.addJob

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.viewModelScope
import com.example.managementsystem.base.BaseViewModel
import com.example.managementsystem.ui.addJob.model.JobModel
import com.example.managementsystem.util.networkUtils.DataResource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class AddJobViewModel @Inject constructor(
    private val addJobRepository: AddJobRepository
) : BaseViewModel() {
    private var _addNewJobResponse: MediatorLiveData<DataResource<String>> = MediatorLiveData()
    val addNewJobResponse: LiveData<DataResource<String>>
        get() = _addNewJobResponse

    var id= ObservableField(0)
    var title = ObservableField("Developer")
    var organizationId = ObservableField("1")
    var location = ObservableField("Pepsicola")
    var jobCategory = ObservableField("Front end")
    var jobType = ObservableField("Permanent")
    var postedDate = ObservableField("20th April")
    var jobExpiryDate = ObservableField("27th April")
    var jobStartDate = ObservableField("2nd May")
    var jobDescription = ObservableField("Front end developer for mobile development")
    var isEditMode :Boolean = false




    fun onSaveClicked() {
        if (!title.get().isNullOrEmpty() &&
            !organizationId.get().isNullOrEmpty() &&
            !location.get().isNullOrEmpty() &&
            !jobCategory.get().isNullOrEmpty() &&
            !jobType.get().isNullOrEmpty() &&
            !postedDate.get().isNullOrEmpty() &&
            !jobExpiryDate.get().isNullOrEmpty() &&
            !jobStartDate.get().isNullOrEmpty() &&
            !jobDescription.get().isNullOrEmpty()
        ) {
            val jobModel = JobModel(
                id= id.get()?:0,
                title = title.get().toString(),
                organizationId = organizationId.get().toString(),
                location = location.get().toString(),
                jobCategory = jobCategory.get().toString(),
                jobType = jobType.get().toString(),
                postedDate = postedDate.get().toString(),
                jobExpiryDate = jobExpiryDate.get().toString(),
                jobStartDate = jobStartDate.get().toString(),
                jobDescription = jobDescription.get().toString(),
                jobAppliedBy = "#"
            )
            viewModelScope.launch {
               if(isEditMode){
                   addJobRepository.updateJobDetails(jobModel).collect {
                       if(it.toLong()>0){
                           _addNewJobResponse.postValue(DataResource.Success())
                           showToast("Job Updated Successfully.")
                       }else{
                           _addNewJobResponse.postValue(DataResource.Error(message = "Failed to create a new job. "))
                       }
                   }
               }else{
                   addJobRepository.saveJobDetails(jobModel).collect {
                       if(it != (-1).toLong()){
                           _addNewJobResponse.postValue(DataResource.Success())
                           showToast("Job Added Successfully.")
                       }else{
                           _addNewJobResponse.postValue(DataResource.Error(message = "Failed to create a new job. "))
                       }
                   }
               }
            }

        } else {
            showToast("Please fill all the fields")
        }

    }
}