package com.example.managementsystem.ui.addJob.model

import java.io.Serializable

data class JobModel(
    val id:Int=0,
    val title:String ,
    val organizationId:String ,
    val location:String,
    val jobCategory:String,
    val jobType:String,
    val postedDate:String ,
    val jobExpiryDate:String ,
    val jobStartDate:String ,
    val jobDescription:String,
    var jobApplied: Boolean = false,
    var jobAppliedBy:String = "",
    var isAlreadyApplied:Boolean = false

):Serializable
