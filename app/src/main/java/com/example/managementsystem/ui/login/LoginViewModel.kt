package com.example.managementsystem.ui.login

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.viewModelScope
import com.example.managementsystem.base.BaseViewModel
import com.example.managementsystem.ui.registration.model.UserModel
import com.example.managementsystem.util.networkUtils.DataResource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class LoginViewModel @Inject constructor(
    private val loginRepository: LoginRepository
) : BaseViewModel() {

    var email  = ObservableField("")
    var password  = ObservableField("")

    private var _loginUserResponse: MediatorLiveData<DataResource<UserModel>> = MediatorLiveData()
    val loginUserResponse: LiveData<DataResource<UserModel>>
        get() = _loginUserResponse


    fun onClickLogin(){
        if(email.get().isNullOrEmpty() || password.get().isNullOrEmpty()){
            showToast("Please provide the required credentials")
        }else{
            viewModelScope.launch {
                loginRepository.loginUser(email.get().toString(),password.get().toString()).collect {
                    it?.let {
                        _loginUserResponse.postValue(DataResource.Success(data = it))
                    }?: _loginUserResponse.postValue(DataResource.Error(message = "Failed to login"))
                }

            }
        }
    }

    fun setDefaultUser() {
        loginRepository.setDefaultUser()
    }

}