package com.example.managementsystem.ui.dashboard.ui.jobs

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.example.managementsystem.R
import com.example.managementsystem.base.BaseFragment
import com.example.managementsystem.databinding.FragmentJobsBinding
import com.example.managementsystem.ui.addJob.AddJobActivity
import com.example.managementsystem.ui.addJob.model.JobModel
import com.example.managementsystem.util.GlobalVariables
import com.example.managementsystem.util.extensions.showConfirmationDialog
import com.example.managementsystem.util.extensions.showToast
import com.example.managementsystem.util.extensions.transitionActivityStart
import com.example.managementsystem.util.networkUtils.DataResource
import dagger.hilt.android.AndroidEntryPoint


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

@AndroidEntryPoint
class JobsFragment : BaseFragment<FragmentJobsBinding, JobsViewModel>() {

    private val jobsViewModel: JobsViewModel by viewModels()

    private lateinit var adapter: HomeAdapter

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(viewDataBinding){
            leftMenuSearch.apply {
                isIconifiedByDefault =false
            }
        }
        initObserver()
    }

    private fun initObserver() {
        jobsViewModel.getJobResponse.observe(requireActivity()) { response ->
            when (response) {
                is DataResource.Loading -> {
                }
                is DataResource.Error -> {
                    viewDataBinding.tvMessage.apply {
                        visibility = View.VISIBLE
                        text = response.message
                    }
                }
                is DataResource.Success -> {
                    viewDataBinding.tvMessage.apply {
                        visibility = View.GONE
                        response.data?.let {
                            setAdapter(it)
                        }

                    }
                }
                else -> {

                }
            }

        }

        jobsViewModel.deleteJobResponse.observe(requireActivity()) { response ->
            when (response) {
                is DataResource.Loading -> {
                }
                is DataResource.Error -> {
                    requireActivity().showToast(response.message)
                }
                is DataResource.Success -> {
                    requireActivity().showToast(response.data)
                    onResume()
                }
                else -> {

                }
            }

        }

        jobsViewModel.applyJobResponse.observe(requireActivity()) { response ->
            when (response) {
                is DataResource.Loading -> {
                }
                is DataResource.Error -> {
                    requireActivity().showToast(response.message)
                }
                is DataResource.Success -> {
                    requireActivity().showToast(response.data)
                    onResume()
                }
                else -> {

                }
            }

        }
    }

    private fun setAdapter(data: List<JobModel>) {
         adapter = HomeAdapter(data, object : OnItemClickListener {
            override fun onItemClick(item: JobModel?) {
                requireActivity().showConfirmationDialog(
                    title = if (GlobalVariables.isAdminMode) "MODIFY JOB" else "APPLY FOR JOB",
                    message = if (GlobalVariables.isAdminMode) "Do you wish to edit this job or delete this job ?" else "Do you wish to apply for the job ? ",
                    positiveAction = {
                        item?.id?.let {
                            if (GlobalVariables.isAdminMode) {
                                jobsViewModel.deleteJob(it)
                            }
                        }

                    },
                    negativeAction = {
                        item?.let {
                            if (GlobalVariables.isAdminMode) {
                                AddJobActivity.startForEditing(requireActivity(), it, true)
                                requireActivity().transitionActivityStart()
                            } else {
                                item.jobApplied = true
                                item.jobAppliedBy = GlobalVariables.number
                                jobsViewModel.applyForJob(item)
                            }
                        }
                    },
                    negativeButton = if (GlobalVariables.isAdminMode) "Edit" else "Apply",
                    positiveButton = if (GlobalVariables.isAdminMode) "Delete" else "Cancel"
                ).show()
            }

        })
        viewDataBinding.recyclerView.adapter = adapter

    }

    override fun onResume() {
        super.onResume()
        jobsViewModel.getJobs()
    }


    override fun getLayoutId(): Int = R.layout.fragment_jobs

    override fun getViewModel(): JobsViewModel = jobsViewModel
}