package com.example.managementsystem.ui.dashboard

import android.content.Context
import com.example.managementsystem.data.DBHelper
import com.example.managementsystem.ui.addJob.model.JobModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn


class DashboardRepository(context: Context) {

    private var dbHelper: DBHelper = DBHelper(context, null)
    fun getJobs(): Flow<List<JobModel>> {
        return flow {
            var jobListResponse = dbHelper.getJobs()
            emit(jobListResponse)
        }.flowOn(Dispatchers.IO)
    }

    fun deleteJob(id:Int):Flow<Int>{
        return flow{
            emit(dbHelper.deleteJob(id))
        }.flowOn(Dispatchers.IO)
    }

    fun applyForJob(jobId:String , appliedBy:String):Flow<Long>{
        return flow{
            emit(dbHelper.insertJobApplied(jobId,appliedBy))
        }.flowOn(Dispatchers.IO)
    }

    fun getAppliedJobs():Flow<List<JobModel>>{
        return flow{
            emit(dbHelper.getAppliedJobs())
        }.flowOn(Dispatchers.IO)
    }
}