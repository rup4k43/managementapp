package com.example.managementsystem.ui.dashboard.ui.appliedJobs

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.viewModelScope
import com.example.managementsystem.base.BaseViewModel
import com.example.managementsystem.ui.addJob.model.JobModel
import com.example.managementsystem.ui.dashboard.DashboardRepository
import com.example.managementsystem.util.networkUtils.DataResource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AppliedJobsViewModel @Inject constructor(
    private val dashboardRepository: DashboardRepository,
) : BaseViewModel() {


    private var _getAppliedJobResponse: MediatorLiveData<DataResource<List<JobModel>>> =
        MediatorLiveData()
    val getAppliedJobResponse: LiveData<DataResource<List<JobModel>>>
        get() = _getAppliedJobResponse

    fun getAppliedJobs() {
        viewModelScope.launch {
            dashboardRepository.getAppliedJobs().collect {
               if(it.isNotEmpty()){
                   _getAppliedJobResponse.postValue(DataResource.Success(it))
               }else{
                   _getAppliedJobResponse.postValue(DataResource.Error(message = "You haven't applied for the jobs yet ! "))
               }
            }
        }
    }
}




