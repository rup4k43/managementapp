package com.example.managementsystem.ui.registration

import android.text.Editable
import android.text.TextWatcher
import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputLayout




/**
 * Binder for benefits text custom style
 * */
@BindingAdapter(
    "app:checkIfFieldIsEmpty",
)
fun checkIfEditTextIsEmpty(view: TextInputLayout, check:Boolean = true) {
    view.editText?.addTextChangedListener(object : TextWatcher {

        override fun afterTextChanged(s: Editable) {}

        override fun beforeTextChanged(s: CharSequence, start: Int,
                                       count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence, start: Int,
                                   before: Int, count: Int) {
            if(s.isEmpty()){
                view.isErrorEnabled = true
                view.error = "This field cannot be empty"
            }else{
                view.error = null
                view.isErrorEnabled = false
            }
        }
    })
}