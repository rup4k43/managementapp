package com.example.managementsystem.ui.login

import android.content.Context
import com.example.managementsystem.data.DBHelper
import com.example.managementsystem.ui.registration.model.UserModel
import com.example.managementsystem.util.networkUtils.DataResource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn


class LoginRepository(context: Context) {
    private var dbHelper: DBHelper = DBHelper(context,null)

    suspend fun loginUser(email:String , password : String):Flow<UserModel?>{
        return flow {
            var checkIfValidUser = dbHelper.checkIfUserIsValid(email,password)
            emit(checkIfValidUser)
        }.flowOn(Dispatchers.IO)

    }

    fun setDefaultUser() {
        dbHelper.registerUser(UserModel(
            firstName = "admin",
            middleName = "-",
            lastName = "",
            mobileNumber = "",
            address = "Random",
            postCode = "12345",
            email = "admin@gmail.com",
            password = "P@ssw0rd",
            isAdmin = true
        ))

        dbHelper.registerUser(UserModel(
            firstName = "Ashwin",
            middleName = "-",
            lastName = "Gurung",
            mobileNumber = "",
            address = "Random",
            postCode = "12345",
            email = "ashwin@gmail.com",
            password = "P@ssw0rd",
            isAdmin = false
        ))

        dbHelper.registerUser(UserModel(
            firstName = "John",
            middleName = "-",
            lastName = "Lastname",
            mobileNumber = "",
            address = "Random",
            postCode = "12345",
            email = "john@gmail.com",
            password = "P@ssw0rd",
            isAdmin = false
        ))
    }
}