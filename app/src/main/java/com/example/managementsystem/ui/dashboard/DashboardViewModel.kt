package com.example.managementsystem.ui.dashboard

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.viewModelScope
import com.example.managementsystem.base.BaseViewModel
import com.example.managementsystem.ui.registration.model.UserModel
import com.example.managementsystem.util.networkUtils.DataResource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class DashboardViewModel @Inject constructor(
    private val dashboardRepository: DashboardRepository
) : BaseViewModel() {



    var email  = ObservableField("")
    var password  = ObservableField("")



}