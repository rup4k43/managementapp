package com.example.managementsystem.ui.registration.model

data class UserModel(
    val firstName:String,
    val middleName:String,
    val lastName : String,
    val mobileNumber:String,
    val address:String,
    val postCode:String,
    val email:String,
    val password:String,
    val isAdmin:Boolean
)
