package com.example.managementsystem.ui.registration

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.activity.viewModels
import com.example.managementsystem.BR
import com.example.managementsystem.R
import com.example.managementsystem.base.BaseActivity
import com.example.managementsystem.databinding.ActivityRegistrationBinding
import com.example.managementsystem.util.extensions.transitionActivityFinish
import com.example.managementsystem.util.networkUtils.DataResource
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class RegistrationActivity : BaseActivity<ActivityRegistrationBinding, RegistrationViewModel>() {

    private val registrationViewModel: RegistrationViewModel by viewModels()

    companion object {
        fun start(context: Context) {
            context.run {
                startActivity(Intent().apply {
                    setClass(this@run, RegistrationActivity::class.java)
                })
            }
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_registration

    override fun getViewModel(): RegistrationViewModel = registrationViewModel

    override fun getBindingDataStringVariable(): Int = BR.dataStrings


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        initObservers()
        initListener()
    }

    private fun initListener() {
        // This overrides the radiogroup onCheckListener
        // This overrides the radiogroup onCheckListener
        viewDataBinding.rgType.setOnCheckedChangeListener { group, checkedId ->
            // This will get the radiobutton that has changed in its check state
            val checkedRadioButton = group.findViewById<View>(checkedId) as RadioButton
            val isChecked = checkedRadioButton.isChecked
            if(checkedRadioButton==viewDataBinding.rbUser){
                if(isChecked){
                    registrationViewModel.isAdmin.set(false)
                }
            }else{
                if(isChecked){
                    registrationViewModel.isAdmin.set(true)
                }
            }
        }
    }

    private fun initViews() {
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
        viewDataBinding.btnRegister.setOnClickListener {
            registrationViewModel.onRegisterClick()
        }

    }

    private fun initObservers() {

        with(registrationViewModel) {
            registerUserResponse.observe(this@RegistrationActivity) {
                when (it) {
                    is DataResource.Loading -> {
                    }
                    is DataResource.Error -> {
                        showToast(it.message)
                    }
                    is DataResource.Success -> {
                        it.data?.let { data->showToast(data) }
                        this@RegistrationActivity.finish()
                        transitionActivityFinish()
                    }
                    else -> {

                    }
                }


            }
        }

    }
}
