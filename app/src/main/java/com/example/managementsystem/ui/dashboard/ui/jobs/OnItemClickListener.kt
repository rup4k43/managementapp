package com.example.managementsystem.ui.dashboard.ui.jobs

import com.example.managementsystem.ui.addJob.model.JobModel

interface OnItemClickListener {
    fun onItemClick(item: JobModel?)
}