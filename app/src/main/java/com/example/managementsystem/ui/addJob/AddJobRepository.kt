package com.example.managementsystem.ui.addJob

import android.content.Context
import com.example.managementsystem.data.DBHelper
import com.example.managementsystem.ui.addJob.model.JobModel
import com.example.managementsystem.ui.registration.model.UserModel
import com.example.managementsystem.util.networkUtils.DataResource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn


class AddJobRepository(context: Context) {
    private var dbHelper: DBHelper = DBHelper(context,null)


     fun saveJobDetails(jobModel: JobModel):Flow<Long>{
        return flow {
            emit(dbHelper.insertJob(jobModel))
        }.flowOn(Dispatchers.IO)
    }

    fun updateJobDetails(jobModel: JobModel,isApplying :Boolean = false):Flow<Int>{
        return flow {
            emit(dbHelper.updateJobByID(jobModel,isApplying))
        }.flowOn(Dispatchers.IO)
    }


}