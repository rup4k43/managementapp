package com.example.managementsystem.ui.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import com.example.managementsystem.BR
import com.example.managementsystem.R
import com.example.managementsystem.base.BaseActivity
import com.example.managementsystem.databinding.ActivityLoginBinding
import com.example.managementsystem.ui.dashboard.DashboardActivity
import com.example.managementsystem.ui.registration.RegistrationActivity
import com.example.managementsystem.util.extensions.transitionActivityStart
import com.example.managementsystem.util.networkUtils.DataResource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity : BaseActivity<ActivityLoginBinding, LoginViewModel>() {

    private val loginViewModel: LoginViewModel by viewModels()

    companion object {
        private const val USERNAME = "username"
        private const val MOBILE_NUMBER = "mobileNumber"
        private const val IS_ADMIN = "is_admin"
        fun start(context: Context) {
            context.run {
                startActivity(Intent().apply {
                    setClass(this@run, LoginActivity::class.java)
                })
            }
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_login

    override fun getViewModel(): LoginViewModel = loginViewModel

    override fun getBindingDataStringVariable(): Int = BR.dataStrings


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        initObservers()
        setDefaultUser()

    }

    private fun setDefaultUser() {
        loginViewModel.setDefaultUser()
    }

    private fun initViews() {
        with(viewDataBinding) {
            signUp.setOnClickListener {
                RegistrationActivity.start(this@LoginActivity)
                transitionActivityStart()
            }
        }

    }

    private fun initObservers() {
        with(loginViewModel) {
            loginUserResponse.observe(this@LoginActivity) {
                when (it) {
                    is DataResource.Loading -> {
                    }
                    is DataResource.Error -> {
                        showToast(it.message)
                    }
                    is DataResource.Success -> {
                        showToast("Successfully logged in .")
                        loginViewModel.email.set("")
                        loginViewModel.password.set("")
                        DashboardActivity.start(
                            this@LoginActivity,
                            it.data?.firstName ?: "",
                            it.data?.mobileNumber ?: "",
                            it.data?.isAdmin ?: false
                        )
                    }
                    else -> {

                    }
                }


            }
        }


    }
}
