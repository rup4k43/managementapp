package com.example.managementsystem.ui.addJob

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import com.example.managementsystem.BR
import com.example.managementsystem.R
import com.example.managementsystem.base.BaseActivity
import com.example.managementsystem.databinding.ActivityAddJobBinding
import com.example.managementsystem.databinding.ActivityLoginBinding
import com.example.managementsystem.ui.addJob.model.JobModel
import com.example.managementsystem.ui.dashboard.DashboardActivity
import com.example.managementsystem.ui.login.LoginViewModel
import com.example.managementsystem.ui.registration.RegistrationActivity
import com.example.managementsystem.util.extensions.transitionActivityFinish
import com.example.managementsystem.util.extensions.transitionActivityStart
import com.example.managementsystem.util.networkUtils.DataResource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AddJobActivity : BaseActivity<com.example.managementsystem.databinding.ActivityAddJobBinding, AddJobViewModel>() {

    private val addJobViewModel: AddJobViewModel by viewModels()



    private val jobModel by lazy {
        intent.getSerializableExtra(JOB_MODEL) as JobModel
    }
    private val isEditMode by lazy {
        intent.getBooleanExtra(IS_EDIT_MODE,false)
    }

    companion object {
        private const val USERNAME = "username"
        private const val MOBILE_NUMBER = "mobileNumber"
        private const val IS_ADMIN = "is_admin"
        private const val JOB_MODEL = "job_model"
        private const val IS_EDIT_MODE = "is_edit_mode"
        fun start(context: Context) {
            context.run {
                startActivity(Intent().apply {
                    setClass(this@run, AddJobActivity::class.java)
                })
            }
        }

        fun startForEditing(context: Context,jobModel: JobModel,isEditingMode:Boolean = false) {
            context.run {
                startActivity(Intent().apply {
                    putExtra(JOB_MODEL,jobModel)
                    putExtra(IS_EDIT_MODE,isEditingMode)
                    setClass(this@run, AddJobActivity::class.java)
                })
            }
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_add_job

    override fun getViewModel(): AddJobViewModel = addJobViewModel

    override fun getBindingDataStringVariable(): Int = BR.dataStrings


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        setButtonText()
        checkMode()
        initObservers()

    }

    private fun setButtonText() {

            viewDataBinding.btnSave.text = if(isEditMode)"Update" else "Save"

    }

    private fun checkMode() {
        if(isEditMode){
            addJobViewModel.id.set(jobModel.id)
            addJobViewModel.isEditMode = true
            addJobViewModel.jobCategory.set(jobModel.jobCategory)
            addJobViewModel.jobDescription.set(jobModel.jobDescription)
            addJobViewModel.jobExpiryDate.set(jobModel.jobExpiryDate)
            addJobViewModel.jobStartDate.set(jobModel.jobStartDate)
            addJobViewModel.jobType.set(jobModel.jobType)
            addJobViewModel.location.set(jobModel.location)
            addJobViewModel.title.set(jobModel.title)
            addJobViewModel.organizationId.set(jobModel.organizationId)
            addJobViewModel.postedDate.set(jobModel.postedDate)
        }
    }

    private fun setupToolbar(){
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
            title = "Add a Job"
        }
    }

    private fun initViews() {
        setupToolbar()
    }

    private fun initObservers() {
        with(addJobViewModel){
            addNewJobResponse.observe(this@AddJobActivity){
                when (it) {
                    is DataResource.Loading -> {
                    }
                    is DataResource.Error -> {
                        showToast(it.message)
                    }
                    is DataResource.Success -> {
                        this@AddJobActivity.finish()
                        transitionActivityFinish()
                    }
                    else -> {

                    }
                }

            }
        }

    }
}
