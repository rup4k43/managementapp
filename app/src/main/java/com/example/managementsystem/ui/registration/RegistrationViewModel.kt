package com.example.managementsystem.ui.registration

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.viewModelScope
import com.example.managementsystem.base.BaseViewModel
import com.example.managementsystem.ui.registration.model.UserModel
import com.example.managementsystem.util.networkUtils.DataResource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class RegistrationViewModel @Inject constructor(
    private val registrationRepository: RegistrationRepository
) : BaseViewModel() {

    private var _registerUserResponse: MediatorLiveData<DataResource<String>> = MediatorLiveData()
    val registerUserResponse: LiveData<DataResource<String>>
        get() = _registerUserResponse

    var isAdmin  = ObservableField(false)
    var firstname = ObservableField("Rupak")
    var middleName = ObservableField("")
    var lastName = ObservableField("Thapa")
    var mobileNumber = ObservableField("+9779808909828")
    var address = ObservableField("Pepsicola")
    var postCode = ObservableField("12345")
    var email = ObservableField("rup4kt43@gmail.com")
    var password = ObservableField("12345678")


    fun onRegisterClick() {
        Log.d("Button Clicked", "Register")

        if (!firstname.get().isNullOrEmpty() &&
            !lastName.get().isNullOrEmpty() &&
            !mobileNumber.get().isNullOrEmpty() &&
            !address.get().isNullOrEmpty() &&
            !postCode.get().isNullOrEmpty() &&
            !email.get().isNullOrEmpty() &&
            !password.get().isNullOrEmpty()
        ) {
            var userModel = UserModel(
                firstName = firstname.get().toString(),
                middleName = middleName.get().toString(),
                lastName = lastName.get().toString(),
                mobileNumber = mobileNumber.get().toString(),
                address = address.get().toString(),
                postCode = postCode.get().toString(),
                email = email.get().toString(),
                password = password.get().toString(),
                isAdmin = isAdmin.get()?:false
            )
            viewModelScope.launch {
                registrationRepository.registerUser(userModel).collect {
                    if(it==(-1).toLong()){
                        _registerUserResponse.postValue(DataResource.Error(message = "Failed to register the user"))
                    }else{
                        _registerUserResponse.postValue(DataResource.Success("User registered sucessfully"))
                    }
                }
            }
        } else {
            showToast("Please fill all the fields.")
        }
    }
}