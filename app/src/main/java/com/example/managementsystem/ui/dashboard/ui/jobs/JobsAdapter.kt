package com.example.managementsystem.ui.dashboard.ui.jobs

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.managementsystem.databinding.ItemJobsBinding
import com.example.managementsystem.ui.addJob.model.JobModel

class HomeAdapter(private val jobList: List<JobModel>,private val listener: OnItemClickListener)
    : RecyclerView.Adapter<HomeAdapter.JobViewHOlder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobViewHOlder {
        val binding = ItemJobsBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return JobViewHOlder(binding)
    }

    override fun getItemCount() = jobList.size

    override fun onBindViewHolder(holder: JobViewHOlder, position: Int) {
        with(holder){
                  var  model  = jobList[position]
               binding.apply {
                   jobModel = model
                   tvStatus.visibility = if(model.isAlreadyApplied) View.VISIBLE else View.GONE
                   jobCard.setOnClickListener {
                       listener.onItemClick(model)
                   }
               }

        }
    }

    inner class JobViewHOlder(val binding: ItemJobsBinding)
        :RecyclerView.ViewHolder(binding.root)

}
