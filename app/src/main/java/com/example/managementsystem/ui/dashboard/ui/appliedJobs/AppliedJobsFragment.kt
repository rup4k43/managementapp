package com.example.managementsystem.ui.dashboard.ui.appliedJobs

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.example.managementsystem.R
import com.example.managementsystem.base.BaseFragment
import com.example.managementsystem.databinding.FragmentAppliedJobsBinding
import com.example.managementsystem.ui.addJob.AddJobActivity
import com.example.managementsystem.ui.addJob.model.JobModel
import com.example.managementsystem.util.GlobalVariables
import com.example.managementsystem.util.extensions.showConfirmationDialog
import com.example.managementsystem.util.extensions.transitionActivityStart
import com.example.managementsystem.util.networkUtils.DataResource
import dagger.hilt.android.AndroidEntryPoint


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

@AndroidEntryPoint
class AppliedJobsFragment : BaseFragment<FragmentAppliedJobsBinding, AppliedJobsViewModel>() {

    private val appliedJobsViewModel: AppliedJobsViewModel by viewModels()

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObserver()
    }

    private fun initObserver() {
        appliedJobsViewModel.getAppliedJobResponse.observe(requireActivity()) { response ->
            when (response) {
                is DataResource.Loading -> {
                }
                is DataResource.Error -> {
                    viewDataBinding.tvMessage.apply {
                        visibility = View.VISIBLE
                        text = response.message
                    }
                }
                is DataResource.Success -> {
                    viewDataBinding.tvMessage.apply {
                        visibility = View.GONE
                        response.data?.let {
                            setAdapter(it)
                        }

                    }
                }
                else -> {

                }
            }

        }

    }

    private fun setAdapter(data: List<JobModel>) {
        val adapter = AppliedJobsAdapter(data, object : OnItemClickListener {
            override fun onItemClick(item: JobModel?) {

            }

        })
        viewDataBinding.recyclerView.adapter = adapter


    }

    override fun onResume() {
        super.onResume()
        appliedJobsViewModel.getAppliedJobs()
    }


    override fun getLayoutId(): Int = R.layout.fragment_applied_jobs

    override fun getViewModel(): AppliedJobsViewModel = appliedJobsViewModel
}