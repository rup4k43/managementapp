package com.example.managementsystem.data

import androidx.databinding.ObservableField

object DataStringsModel {
    // Messages
    var ok = ObservableField("Ok")
    var cancel = ObservableField("Cancel")
    val notNow = ObservableField("Not now")
    val settings = ObservableField("Settings")

}