package com.example.managementsystem.data

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.example.managementsystem.ui.addJob.model.JobModel
import com.example.managementsystem.ui.registration.model.UserModel
import com.example.managementsystem.util.GlobalVariables


class DBHelper(context: Context, factory: SQLiteDatabase.CursorFactory?) :
    SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {

    // below is the method for creating a database by a sqlite query
    override fun onCreate(db: SQLiteDatabase) {
        // below is a sqlite query, where column names
        // along with their data types is given
        val userTableQuery = ("CREATE TABLE " + TABLE_USER + " ("
                + COL_ID + " INTEGER PRIMARY KEY, " +
                COL_FIRST_NAME + " TEXT," +
                COL_MIDDLE_NAME + " TEXT," +
                COL_LAST_NAME + " TEXT," +
                COL_MOBILE_NUMBER + " TEXT UNIQUE," +
                COL_ADDRESS + " TEXT," +
                COL_POSTCODE + " TEXT," +
                COL_EMAIL + " TEXT," +
                COL_IS_ADMIN + " INT, " +
                COL_PASSWORD + " TEXT" + ")")

        val jobTableQuery = ("CREATE TABLE " + TABLE_JOB + " ("
                + COL_ID + " INTEGER PRIMARY KEY, " +
                COL_ORGANIZATION_ID + " TEXT, " +
                COL_JOB_TITLE + " TEXT," +
                COL_LOCATION + " TEXT," +
                COL_JOB_CATEGORY + " TEXT," +
                COL_JOB_TYPE + " TEXT," +
                COL_POSTED_DATE + " TEXT," +
                COL_EXPIRY_DATE + " TEXT," +
                COL_START_DATE + " TEXT," +
                COL_JOB_APPLIED_BY + " TEXT," +
                COL_DESCRIPTION + " TEXT" + ")")

        val jobAppliedTableQuery = ("CREATE TABLE " + TABLE_JOB_APPLIED + " ("
                + COL_ID + " INTEGER , " +
                COL_JOB_APPLIED_BY + " TEXT" + ")")

        // we are calling sqlite
        // method for executing our query
        db.execSQL(userTableQuery)
        db.execSQL(jobTableQuery)
        db.execSQL(jobAppliedTableQuery)
    }

    fun registerUser(userModel: UserModel): Long {
        val values = ContentValues()
        values.put(COL_FIRST_NAME, userModel.firstName)
        values.put(COL_MIDDLE_NAME, userModel.middleName)
        values.put(COL_LAST_NAME, userModel.lastName)
        values.put(COL_MOBILE_NUMBER, userModel.mobileNumber)
        values.put(COL_ADDRESS, userModel.address)
        values.put(COL_POSTCODE, userModel.postCode)
        values.put(COL_EMAIL, userModel.email)
        values.put(COL_PASSWORD, userModel.password)
        values.put(COL_IS_ADMIN, if (userModel.isAdmin) 1 else 0)

        val db = this.writableDatabase
        //  db.close()
        return db.insert(TABLE_USER, null, values)
    }

    fun insertJob(jobModel: JobModel): Long {
        val values = ContentValues()
        values.put(COL_ORGANIZATION_ID, jobModel.organizationId)
        values.put(COL_JOB_TITLE, jobModel.title)
        values.put(COL_LOCATION, jobModel.location)
        values.put(COL_JOB_CATEGORY, jobModel.jobCategory)
        values.put(COL_JOB_TYPE, jobModel.jobType)
        values.put(COL_POSTED_DATE, jobModel.postedDate)
        values.put(COL_EXPIRY_DATE, jobModel.jobExpiryDate)
        values.put(COL_START_DATE, jobModel.jobStartDate)
        values.put(COL_DESCRIPTION, jobModel.jobDescription)
        values.put(COL_JOB_APPLIED_BY,"#")

        val db = this.writableDatabase
        //  db.close()
        return db.insert(TABLE_JOB, null, values)
    }


    fun insertJobApplied(id: String, appliedBy: String): Long {
        val values = ContentValues()
        values.put(COL_ID, id)
        values.put(COL_JOB_APPLIED_BY, appliedBy)
        val db = this.writableDatabase
        //  db.close()
        return if(checkIfAlreadyApplied(id,appliedBy)){
            -3
        }else{
            db.insert(TABLE_JOB_APPLIED, null, values)
        }
    }

    fun checkIfAlreadyApplied( id:String,appliedBy: String):Boolean{
        val db = this.readableDatabase
        var cursor: Cursor? = null
        val sql =
            "SELECT * FROM $TABLE_JOB_APPLIED WHERE $COL_JOB_APPLIED_BY='$appliedBy' AND $COL_ID = ${id.toInt()}"
        Log.e("Query", sql)
        cursor = db.rawQuery(sql, null)
        return cursor.count > 0
    }


    @SuppressLint("Range")
    fun getAppliedJobs(): List<JobModel> {
        val jobList: ArrayList<JobModel> = arrayListOf()
        val db = this.readableDatabase
        val sql =
            "SELECT * FROM $TABLE_JOB  t1  JOIN $TABLE_JOB_APPLIED t2  ON t1.$COL_ID =  t2.$COL_ID"
        var cursor: Cursor? = null
        try {
            cursor = db.rawQuery(sql, null)
        } catch (e: SQLiteException) {
            db.execSQL(sql)
        }
        cursor?.let {
            if (it.moveToFirst()) {
                do {
                    val jobModel = JobModel(
                        id = cursor.getInt(cursor.getColumnIndex(COL_ID)),
                        organizationId = cursor.getString(cursor.getColumnIndex(COL_ORGANIZATION_ID)),
                        title = cursor.getString(cursor.getColumnIndex(COL_JOB_TITLE)),
                        location = cursor.getString(cursor.getColumnIndex(COL_LOCATION)),
                        jobCategory = cursor.getString(cursor.getColumnIndex(COL_JOB_CATEGORY)),
                        jobType = cursor.getString(cursor.getColumnIndex(COL_JOB_TYPE)),
                        postedDate = cursor.getString(cursor.getColumnIndex(COL_POSTED_DATE)),
                        jobExpiryDate = cursor.getString(cursor.getColumnIndex(COL_EXPIRY_DATE)),
                        jobStartDate = cursor.getString(cursor.getColumnIndex(COL_START_DATE)),
                        jobDescription = cursor.getString(cursor.getColumnIndex(COL_DESCRIPTION)),
                    )
                    jobList.add(jobModel)

                } while (cursor.moveToNext())
            }
        }
        return jobList
    }

    fun updateJobByID(jobModel: JobModel, isApplying: Boolean = false): Int {
        val values = ContentValues()
        values.put(COL_ORGANIZATION_ID, jobModel.organizationId)
        values.put(COL_JOB_TITLE, jobModel.title)
        values.put(COL_LOCATION, jobModel.location)
        values.put(COL_JOB_CATEGORY, jobModel.jobCategory)
        values.put(COL_JOB_TYPE, jobModel.jobType)
        values.put(COL_POSTED_DATE, jobModel.postedDate)
        values.put(COL_EXPIRY_DATE, jobModel.jobExpiryDate)
        values.put(COL_START_DATE, jobModel.jobStartDate)
        values.put(COL_DESCRIPTION, jobModel.jobDescription)
        values.put(COL_JOB_APPLIED_BY, jobModel.jobAppliedBy)
        return this.writableDatabase.update(
            TABLE_JOB,
            values,
            "id=?",
            arrayOf(jobModel.id.toString())
        )


    }


    @SuppressLint("Range")
    fun checkIfUserIsValid(email: String, password: String): UserModel? {
        if (checkIfUserExist(email, password)) {
            val db = this.readableDatabase
            var cursor: Cursor? = null
            val sql =
                "SELECT * FROM $TABLE_USER WHERE $COL_EMAIL='$email'AND $COL_PASSWORD= '$password'"
            Log.e("Query", sql)
            cursor = db.rawQuery(sql, null)
            cursor.moveToFirst()
            if (cursor.count > 0) {
                val isAdmin: Int = cursor.getInt(cursor.getColumnIndex(COL_IS_ADMIN))
                return UserModel(
                    firstName = cursor.getString(cursor.getColumnIndex(COL_FIRST_NAME)),
                    middleName = cursor.getString(cursor.getColumnIndex(COL_MIDDLE_NAME)),
                    lastName = cursor.getString(cursor.getColumnIndex(COL_LAST_NAME)),
                    address = cursor.getString(cursor.getColumnIndex(COL_ADDRESS)),
                    mobileNumber = cursor.getString(cursor.getColumnIndex(COL_MOBILE_NUMBER)),
                    postCode = cursor.getString(cursor.getColumnIndex(COL_POSTCODE)),
                    email = cursor.getString(cursor.getColumnIndex(COL_EMAIL)),
                    isAdmin = isAdmin == 1,
                    password = ""
                )
            } else {
                return null
            }
        }
        return null
    }

    fun checkIfUserExist(email: String, password: String): Boolean {
        val db = this.readableDatabase
        var cursor: Cursor? = null
        val sql =
            "SELECT * FROM $TABLE_USER WHERE $COL_EMAIL='$email'AND $COL_PASSWORD= '$password'"
        Log.e("Query", sql)
        cursor = db.rawQuery(sql, null)
        return cursor.count > 0
    }

    fun checkIfJobAppliedTableIsEmpty(): Boolean {
        val db = this.readableDatabase
        var cursor: Cursor? = null
        val sql =
            "SELECT * FROM $TABLE_JOB_APPLIED"
        Log.e("Query", sql)
        cursor = db.rawQuery(sql, null)
        return cursor.count <= 0
    }

    @SuppressLint("Range")
    fun getJobs(): List<JobModel> {
        Log.e("GetJobS Hit", "here")
        val jobList: ArrayList<JobModel> = arrayListOf()
        val db = this.readableDatabase
        val sql =  if(GlobalVariables.isAdminMode || checkIfJobAppliedTableIsEmpty()){
            "SELECT * FROM $TABLE_JOB "
        }else{
            //"SELECT * FROM $TABLE_JOB  t1 LEFT JOIN $TABLE_JOB_APPLIED t2  ON t1.$COL_JOB_APPLIED_BY !=  t2.$COL_JOB_APPLIED_BY"
            "SELECT * FROM $TABLE_JOB "
        }
        Log.e("SQL QUERY : ", sql)
        var cursor: Cursor? = null
        try {
            cursor = db.rawQuery(sql, null)
        } catch (e: SQLiteException) {
            db.execSQL(sql)
        }
        cursor?.let {
            if (it.moveToFirst()) {
                do {
                    var checkIfAlreadyApplied = checkIfAlreadyApplied((cursor.getInt(cursor.getColumnIndex(COL_ID))).toString(),GlobalVariables.number)
                    val jobModel = JobModel(
                        id = cursor.getInt(cursor.getColumnIndex(COL_ID)),
                        organizationId = cursor.getString(cursor.getColumnIndex(COL_ORGANIZATION_ID)),
                        title = cursor.getString(cursor.getColumnIndex(COL_JOB_TITLE)),
                        location = cursor.getString(cursor.getColumnIndex(COL_LOCATION)),
                        jobCategory = cursor.getString(cursor.getColumnIndex(COL_JOB_CATEGORY)),
                        jobType = cursor.getString(cursor.getColumnIndex(COL_JOB_TYPE)),
                        postedDate = cursor.getString(cursor.getColumnIndex(COL_POSTED_DATE)),
                        jobExpiryDate = cursor.getString(cursor.getColumnIndex(COL_EXPIRY_DATE)),
                        jobStartDate = cursor.getString(cursor.getColumnIndex(COL_START_DATE)),
                        jobDescription = cursor.getString(cursor.getColumnIndex(COL_DESCRIPTION)),
                        isAlreadyApplied = checkIfAlreadyApplied
                    )
                    jobList.add(jobModel)

                } while (cursor.moveToNext())
            }
        }

        return jobList
    }

    fun deleteJob(id: Int): Int {
        return this.writableDatabase.delete(TABLE_JOB, "id=?", arrayOf(id.toString()))
    }

    override fun onUpgrade(db: SQLiteDatabase, p1: Int, p2: Int) {
        // this method is to check if table already exists
        db.execSQL("DROP TABLE IF EXISTS $TABLE_USER")
        onCreate(db)
    }




    companion object {
        private const val DATABASE_NAME = "management_system"

        // below is the variable for database version
        private const val DATABASE_VERSION = 1

        // User table
        private const val TABLE_USER = "table_user"
        private const val COL_ID = "id"
        private const val COL_FIRST_NAME = "firstName"
        private const val COL_MIDDLE_NAME = "middleName"
        private const val COL_LAST_NAME = "lastName"
        private const val COL_MOBILE_NUMBER = "mobileNumber"
        private const val COL_ADDRESS = "address"
        private const val COL_POSTCODE = "postCode"
        private const val COL_EMAIL = "email"
        private const val COL_PASSWORD = "password"
        private const val COL_IS_ADMIN = "is_admin"

        //JOB Table
        private const val TABLE_JOB = "table_job"
        private const val COL_JOB_TITLE = "job_title"
        private const val COL_ORGANIZATION_ID = "organization_id"
        private const val COL_LOCATION = "location"
        private const val COL_JOB_CATEGORY = "job_category"
        private const val COL_JOB_TYPE = "job_type"
        private const val COL_POSTED_DATE = "posted_date"
        private const val COL_EXPIRY_DATE = "expiry_date"
        private const val COL_START_DATE = "start_date"
        private const val COL_DESCRIPTION = "description"

        //JOB APPLIED TABLE
        private const val TABLE_JOB_APPLIED = "table_job_applied"
        private const val COL_JOB_ID = "job_id"
        private const val COL_JOB_APPLIED_BY = "job_applied_by"

    }
}